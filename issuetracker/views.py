from django.shortcuts import get_object_or_404, render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.views import generic
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .models import Issue, Note
from issuetracker.forms import NameForm, IssueForm


from django import template
from issuetracker.models import Issue
register = template.Library()


@register.simple_tag
def active_issue_count():
    return 5 #Issue.objects.filter(active=True).count()




def login_user(request):
    if request.user.is_authenticated():
        messages.add_message(request, messages.ERROR, "You have been logged out")
    logout(request)
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/issuetracker/')
        else:
            messages.add_message(request, messages.ERROR, "Incorrect username or password")
    return render_to_response('issuetracker/login.html', context_instance=RequestContext(request))


def register_user(request):
    logout(request)
    username = password = email = first_name = last_name = ''
    if request.POST:
        if request.POST['password1'] != request.POST['password2']:
            messages.add_message(request, messages.ERROR, "Passwords do not match")
            return render_to_response('issuetracker/register.html', context_instance=RequestContext(request))
        # TODO: Check to make sure username and email are not taken already
        username = request.POST.get('username', None)
        password = request.POST.get('password1', None)
        email = request.POST.get('email', None)
        first_name = request.POST.get('first_name', None)
        last_name = request.POST.get('last_name', None)
        if User.objects.filter(username=username).exists():
            messages.add_message(request, messages.ERROR, "An account with that username already exists.")
        else:
            user = User.objects.create_user(username=username, email=email, password=password, first_name=first_name, last_name=last_name)
            if user is not None:
                messages.add_message(request, messages.SUCCESS, "Your account has been created!")
                if user.is_active:
                    # login(request, user)
                    return HttpResponseRedirect('/accounts/login/')
            else:
                messages.add_message(request, messages.ERROR, "There was an issue creating your account.")
    return render_to_response('issuetracker/register.html', context_instance=RequestContext(request))


def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = IssueForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = IssueForm(instance=Issue.objects.get(pk=2))

    return render(request, 'issuetracker/name.html', {'form': form})


@login_required(login_url='/accounts/login/')
def index(request):
    return HttpResponseRedirect(reverse("issuetracker:index"))


# @login_required(login_url='/accounts/login')
class IndexView(generic.ListView):
    template_name = 'issuetracker/index.html'
    context_object_name = 'issue_list'

    def get_queryset(self):
        # return Issue.objects.filter(resolved=0).order_by('-pub_modified')
        return Issue.objects.filter(active=True).order_by('-pub_modified')


@login_required(login_url='/accounts/login/')
class IssueView(generic.DetailView):
    model = Issue
    template_name = 'issuetracker/detail.html'

@login_required(login_url='/accounts/login/')
def issue(request, issue_id):
    issue = get_object_or_404(Issue, pk=issue_id)
    return render(request, 'issuetracker/detail.html', { 'issue': issue })


class DeletedView(generic.ListView):
    template_name = 'issuetracker/deleted.html'
    context_object_name = 'issue_list'

    def get_queryset(self):
        return Issue.objects.filter(active=False).order_by('-pub_modified')


@login_required(login_url='/accounts/login/')
def newissue(request):
    if request.method == 'POST' and request.POST:
        try:
            issue = Issue()
            issue.user = request.user
            issue.issue_text = request.POST['issue_text']
            if len(issue.issue_text) > 0:
                issue.save()
                messages.add_message(request,messages.SUCCESS, "Issue added successfully")
                return HttpResponseRedirect(reverse('issuetracker:index'))
            else:
                messages.add_message(request,messages.ERROR, "Please enter an issue")
        except:
            messages.add_message(request,messages.WARNING, "Something went wrong")
    return render(request, 'issuetracker/newissue.html')


@login_required(login_url='/accounts/login/')
def deleteissue(request, issue_id):
    issue = get_object_or_404(Issue, pk=issue_id)
    issue.active = False
    issue.save()
    return HttpResponseRedirect(reverse('issuetracker:index'))


@login_required(login_url='/accounts/login/')
def undeleteissue(request, issue_id):
    issue = get_object_or_404(Issue, pk=issue_id)
    issue.active = True
    issue.save()
    return HttpResponseRedirect(reverse('issuetracker:deleted'))


@login_required(login_url='/accounts/login/')
def newnote(request, issue_id):
    issue = get_object_or_404(Issue, pk=issue_id)
    if request.method == 'POST' and request.POST:
        try:
            note = Note()
            note.user = request.user
            note.note_text = request.POST['note_text']
            if len(note.note_text) > 0:
                note.issue = issue
                note.save()
                messages.add_message(request,messages.SUCCESS, "Note added successfully")
                return HttpResponseRedirect(reverse('issuetracker:detail', args=(issue_id,)))
            else:
                messages.add_message(request,messages.ERROR, "Please enter a note")
        except:
            messages.add_message(request,messages.WARNING, "Something went wrong")
    return HttpResponseRedirect(reverse('issuetracker:detail', args=(issue_id,)))


@login_required(login_url='/accounts/login/')
def saveissue(request, issue_id):
    issue = get_object_or_404(Issue, pk=issue_id)
    try:
        issue.issue_text = request.POST['issue_text']
        if 'resolved' not in request.POST:
            issue.resolved = False
        elif request.POST['resolved'] == "on":
            issue.resolved = True
        issue.save()
    except (KeyError, Issue.DoesNotExist):
        messages.add_message(request, messages.ERROR, "You didn't select an issue.")
        return render(request, 'issuetracker/detail.html', { 'issue': issue })
    else:
        # return render(request, 'issuetracker/detail.html', {
        #     'issue': issue,
        #     'success_message': "Issue saved successfully.",
        # })
        messages.add_message(request, messages.SUCCESS, "Issue saved successfully")
        return HttpResponseRedirect(reverse('issuetracker:index'))

# class ResultsView(generic.DetailView):
#     model = Question
#     template_name = 'polls/results.html'
#
# def vote(request, question_id):
#     question = get_object_or_404(Question, pk=question_id)
#     try:
#         selected_choice = question.choice_set.get(pk=request.POST['choice'])
#     except (KeyError, Choice.DoesNotExist):
#         # Redisplay the question voting form.
#         return render(request, 'polls/detail.html', {
#             'question': question,
#             'error_message': "You didn't select a choice.",
#         })
#     else:
#         selected_choice.votes += 1
#         selected_choice.save()
#         # Always return an HttpResponseRedirect after successfully dealing
#         # with POST data. This prevents data from being posted twice if a
#         # user hits the Back button.
#         return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))