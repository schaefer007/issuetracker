from django import template
from issuetracker.models import Issue
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.simple_tag
@stringfilter
def issue_count(value, current_user=None):
    if value == 'active':
        return Issue.objects.filter(active=True).count()
    elif value == 'inactive':
        return Issue.objects.filter(active=False).count()
    elif value == 'current_user':
        return Issue.objects.filter(user=current_user).count()


@register.inclusion_tag('weather.html')
def array_test():
    return {'number':5}
