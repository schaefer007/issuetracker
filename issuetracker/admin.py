from django.contrib import admin

# Register your models here.
from .models import Issue, Note


class IssueInline(admin.TabularInline):
    model = Issue
    extra = 3


class IssueAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['issue_text']}),
        ('Published Date',   {'fields': ['pub_date']}),
        ('Last Modified',    {'fields': ['pub_modified']})
    ]
    list_display = ('issue_text', 'pub_date', 'pub_modified')
    list_filter = ['pub_date']
    search_fields = ['issue_text']

admin.site.register(Issue, IssueAdmin)
admin.site.register(Note)