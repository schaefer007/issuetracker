from django.conf.urls import url
from . import views

app_name = 'issuetracker'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    # url(r'^issue/(?P<pk>[0-9]+)/$', views.IssueView.as_view(), name='detail'),
    url(r'^issue/(?P<issue_id>[0-9]+)/$', views.issue, name='detail'),
    url(r'^issue/add/$',views.newissue, name='newissue'),
    url(r'^issue/(?P<issue_id>[0-9]+)/save/$', views.saveissue, name='saveissue'),
    url(r'^issue/(?P<issue_id>[0-9]+)/delete/$', views.deleteissue, name='deleteissue'),
    url(r'^issue/(?P<issue_id>[0-9]+)/undelete/$', views.undeleteissue, name='undeleteissue'),
    url(r'^issue/(?P<issue_id>[0-9]+)/note/add/$', views.newnote, name='newnote'),
    url(r'^deleted/$', views.DeletedView.as_view(), name='deleted')
    # url(r'^nameform/$', views.get_name, name='get_name')
    # url(r'^$', views.index, name='index')
]