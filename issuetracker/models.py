from django.db import models
from django.contrib.auth.models import User


class Issue(models.Model):
    issue_text = models.TextField(verbose_name="Issue")
    user = models.ForeignKey(User)
    resolved = models.BooleanField(default=False)
    pub_date = models.DateTimeField(auto_now_add=True, verbose_name='Date Added')
    pub_modified = models.DateTimeField(auto_now=True, verbose_name='Last Modified')
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.issue_text


class Note(models.Model):
    issue = models.ForeignKey(Issue, on_delete=models.CASCADE)
    user = models.ForeignKey(User)
    note_text = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)
    pub_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.note_text