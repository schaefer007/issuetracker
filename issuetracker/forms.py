from django import forms
from issuetracker.models import Issue
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)


class IssueForm(forms.ModelForm):
    class Meta:
        model = Issue
        fields = ['issue_text']