"""arbsol URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import issuetracker.views
from django.contrib.auth import views

urlpatterns = [
    # url(r'^/', 'django.contrib.auth.login', {'template_name':'admin/login.html'}),
    url(r'^$', issuetracker.views.index),
    url(r'^accounts/login/$', issuetracker.views.login_user),
    url(r'^accounts/logout/$', views.logout, {'template_name': 'issuetracker/index.html'}),
    url(r'^accounts/register/$', issuetracker.views.register_user),
    # url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'admin/login.html'}),
    # url(r'^accounts/changepassword/$', 'django.contrib.auth.views.password_change'),
    # url(r'^accounts/profile/$', 'issuetracker.views.index'),
    # url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^issuetracker/', include('issuetracker.urls')),
    url(r'^admin/', admin.site.urls),
]
